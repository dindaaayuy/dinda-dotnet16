﻿/*Console.WriteLine("-------");
Console.WriteLine("|  1  |");
Console.WriteLine("-------");

Console.WriteLine("=======");
Console.WriteLine("|| 1 ||");
Console.WriteLine("=======");

var loop = true;
while (loop)
{
    var key = Console.ReadKey().Key;
    switch (key)
    {
        case ConsoleKey.DownArrow:
            Console.WriteLine("DownArrow");
            break;

        case ConsoleKey.UpArrow:
            Console.WriteLine("UpArrow");
            break;

        case ConsoleKey.RightArrow:
            Console.WriteLine("RightArrow");
            break;

        case ConsoleKey.LeftArrow:
            Console.WriteLine("LeftArrow");
            break;

        case ConsoleKey.Escape:
            loop = false;
            break;
    }

}*/


/*  Console.WriteLine("StaticValue={0}", ContohStatic.StaticValue);
    Console.WriteLine("ConstValue={0}", ContohStatic.ConstValue);

    ContohStatic.StaticValue = 50;
    Console.WriteLine("StaticValue={0}", ContohStatic.StaticValue);
    ContohStatic.ConstValue = 100;
    Console.WriteLine("ConstValue={0}", ContohStatic.ConstValue);

    ContohStatic _contohStatic = new ContohStatic();
    Console.WriteLine("DefinedValue={0}", _contohStatic.DefinedValue);*/


static class Program
{

    const int COLS = 6;
    const int ROWS = 6;

    const string BLUE = "B";
    const string GREEN = "G";
    const string RED = "R";
    const string ORANGE = "O";
    const string PURPLE = "P";
    const string EMPTY = " ";

    const int ITEM_WIDTH = 9;
    const int ITEM_HEIGHT = 4;

    static int SCREEN_WIDTH = Console.WindowWidth;
    static int SCREEN_HEIGHT = Console.WindowHeight;

    static string[,] data = new string[ROWS, COLS];
    static string[,] backup = new string[ROWS, COLS];


    static Random random = new Random();

    static int CURR_ROW = 0;
    static int CURR_COL = 0;
    static int SELECTED_ROW = 0;
    static int SELECTED_COL = 0;

    const int SELECTION_MODE_UNSELECTED = 1;
    const int SELECTION_MODE_ON_SELECTION = 2;


    static int CURR_SELECTION_MODE = SELECTION_MODE_UNSELECTED;

    static string randomize()
    {
        var result = random.Next(1, 5);
        switch (result)
        {
            case 1: return BLUE;
            case 2: return GREEN;
            case 3: return RED;
            case 4: return ORANGE;
            case 5: return PURPLE;
        }
        return "";
    }

    static void initData()
    {
        for (int i = 0; i < ROWS; i++)
        {
            for (int j = 0; j < COLS; j++)
            {
                data[i, j] = randomize();
            }
        }
    }


    static int getStartXPosition()
    {
        var halfScreen = SCREEN_WIDTH / 2;

        var halfItemsWidth = (COLS * ITEM_WIDTH) / 2;

        return halfScreen = halfScreen - halfItemsWidth;

    }

    static int getStartYPosition()
    {
        var halfScreen = SCREEN_HEIGHT / 2;

        var halfItemsHeight = (ROWS * ITEM_HEIGHT) / 2;

        return halfScreen = halfScreen - halfItemsHeight;

    }

    static void printEmptyX(int startPosition)
    {
        for (int j = 0; j < startPosition; j++)
        {
            Console.Write(" ");
        }
    }

    static void printEmptyY(int startPosition)
    {
        for (int j = 0; j < startPosition; j++)
        {
            Console.WriteLine();
        }
    }

    static void printHLine(int startPosition, int i)
    {
        printEmptyX(startPosition);
        for (int j = 0; j < COLS; j++)
        {
            if (i == CURR_ROW && j == CURR_COL)
            {
                Console.Write("=======  ");
            }
            else
            {
                Console.Write("-------  ");
            }
        }
        Console.WriteLine();
    }

    static void printData()
    {
        /*
        
         -----  -----  -------  -----  -----
         | G |  | B |  |  O  |  | P |  | R |
         -----  -----  -------  -----  -----

         -----  -----  =======  -----  -----
         | G |  | B |  || O ||  | P |  | R |
         -----  -----  =======  -----  -----

         -----  -----  -----  -----  -----
         | G |  | B |  | O |  | P |  | R |
         -----  -----  -----  -----  -----

         -----  -----  -----  -----  -----
         | G |  | B |  | O |  | P |  | R |
         -----  -----  -----  -----  -----

         -----  -----  -----  -----  -----
         | G |  | B |  | O |  | P |  | R |
         -----  -----  -----  -----  -----


         */

        var startXPosition = getStartXPosition();
        var startYPosition = getStartYPosition();

        printEmptyY(startYPosition);

        for (int i = 0; i < ROWS; i++)
        {

            printHLine(startXPosition, i);

            printEmptyX(startXPosition);
            for (int j = 0; j < COLS; j++)
            {
                if (i == CURR_ROW && j == CURR_COL)
                {
                    Console.Write("|| " + data[i, j] + " ||  ");
                }
                else
                {
                    Console.Write("|  " + data[i, j] + "  |  ");
                }
            }
            Console.WriteLine();

            printHLine(startXPosition, i);

            Console.WriteLine();
            Console.WriteLine();

        }

    }


    static void moveUp()
    {
        if (CURR_SELECTION_MODE == SELECTION_MODE_UNSELECTED)
        {
            if (CURR_ROW > 0)
            {
                CURR_ROW--;
            }

        }
        else if (CURR_SELECTION_MODE == SELECTION_MODE_ON_SELECTION)
        {

            if (CURR_ROW > 0)
            {
                var temp = data[CURR_ROW, CURR_COL];
                data[CURR_ROW, CURR_COL] = data[CURR_ROW - 1, CURR_COL];
                data[CURR_ROW - 1, CURR_COL] = temp;

                CURR_ROW--;
            }
        }

    }

    static void moveDown()
    {
        if (CURR_SELECTION_MODE == SELECTION_MODE_UNSELECTED)
        {
            if (CURR_ROW < (ROWS - 1))
            {
                CURR_ROW++;
            }

        }
        else if (CURR_SELECTION_MODE == SELECTION_MODE_ON_SELECTION)
        {

            if (CURR_ROW < (ROWS - 1))
            {
                var temp = data[CURR_ROW, CURR_COL];
                data[CURR_ROW, CURR_COL] = data[CURR_ROW + 1, CURR_COL];
                data[CURR_ROW + 1, CURR_COL] = temp;

                CURR_ROW++;
            }
        }
    }

    static void moveLeft()
    {
        if (CURR_SELECTION_MODE == SELECTION_MODE_UNSELECTED)
        {
            if (CURR_COL > 0)
            {
                CURR_COL--;
            }
        }
        else if (CURR_SELECTION_MODE == SELECTION_MODE_ON_SELECTION)
        {
            if (CURR_COL > 0)
            {
                var temp = data[CURR_ROW, CURR_COL];
                data[CURR_ROW, CURR_COL] = data[CURR_ROW, CURR_COL - 1];
                data[CURR_ROW, CURR_COL - 1] = temp;

                CURR_COL--;
            }
        }

    }

    static void moveRight()
    {
        if (CURR_SELECTION_MODE == SELECTION_MODE_UNSELECTED)
        {
            if (CURR_COL < (COLS - 1))
            {
                CURR_COL++;
            }
        }
        else if (CURR_SELECTION_MODE == SELECTION_MODE_ON_SELECTION)
        {
            if (CURR_COL < (COLS - 1))
            {
                var temp = data[CURR_ROW, CURR_COL];
                data[CURR_ROW, CURR_COL] = data[CURR_ROW, CURR_COL + 1];
                data[CURR_ROW, CURR_COL + 1] = temp;

                CURR_COL++;

            }
        }

    }


    static void match()
    {
        if (CURR_ROW == 0)
        {
            if (CURR_COL == 0)
            {
                int cnt = 0;
                for (int j = CURR_COL + 1; j < COLS; j++)
                {
                    if (data[CURR_ROW, CURR_COL] == data[CURR_ROW, j])
                    {
                        cnt++;
                    }
                }

            }
        }
    }

    static void backupData()
    {
        for (int i = 0; i < ROWS; i++)
        {
            for (int j = 0; j < COLS; j++)
            {
                backup[i, j] = data[i, j];
            }
        }
    }

    static void restoreData()
    {
        for (int i = 0; i < ROWS; i++)
        {
            for (int j = 0; j < COLS; j++)
            {
                data[i, j] = backup[i, j];
            }
        }
    }

    static void startOrStopMoving()
    {
        if (CURR_SELECTION_MODE == SELECTION_MODE_UNSELECTED)
        {
            CURR_SELECTION_MODE = SELECTION_MODE_ON_SELECTION;
            backupData();

        }
        else
        {
            CURR_SELECTION_MODE = SELECTION_MODE_UNSELECTED;

        }
    }

    static void cancelMoving()
    {
        if (CURR_SELECTION_MODE == SELECTION_MODE_ON_SELECTION)
        {
            restoreData();
            CURR_SELECTION_MODE = SELECTION_MODE_UNSELECTED;
        }
    }

    static ConsoleKey readInput()
    {

        var key = Console.ReadKey();
        switch (key.Key)
        {
            case ConsoleKey.UpArrow:
                moveUp();
                break;

            case ConsoleKey.DownArrow:
                moveDown();
                break;

            case ConsoleKey.LeftArrow:
                moveLeft();
                break;

            case ConsoleKey.RightArrow:
                moveRight();
                break;

            case ConsoleKey.Enter:
                startOrStopMoving();
                break;


            case ConsoleKey.Escape:
                cancelMoving();
                break;



        }
        return key.Key;
    }

    static void Main(string[] args)
    {

        initData();

        bool loop = true;
        while (loop)
        {
            Console.Clear();
            printData();
            var key = readInput();
            /*if(key == ConsoleKey.Escape && CURR_SELECTION_MODE == SELECTION_MODE_UNSELECTED)
            {
                loop = false;
            }*/
        }


    }

}
