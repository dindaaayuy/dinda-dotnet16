﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("===================================================================================================================");
Console.WriteLine("Daftar Karyawan");
Console.WriteLine("===================================================================================================================");
Console.WriteLine("Kode Karyawan     |   Nama Karyawan    |    Mulai Bekerja     |    Masih Bekerja     |    Tgl Keluar     ");
Console.WriteLine("===================================================================================================================");
Console.WriteLine("EE001             |   Faisal           |    10-Feb-2022       |    Masih             |                   ");
Console.WriteLine("EE002             |   Vido             |    11-Feb-2022       |    Masih             |                   ");
Console.WriteLine("EE003             |   Dinda            |    12-Feb-2022       |    Masih             |                   ");
Console.WriteLine("EE004             |   Dwi Putri        |    12-Feb-2022       |    Masih             |                   ");
Console.WriteLine("EE005             |   Susanto          |    12-Feb-2022       |    Masih             |                   ");
Console.WriteLine("EE006             |   Fathan           |    12-Feb-2022       |    Masih             |                   ");
Console.WriteLine("...");
Console.WriteLine("...");
Console.WriteLine("EE011             |   Riska            |    12-Feb-2022       |    Masih             |    31-Des-2022    ");